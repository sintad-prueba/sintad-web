import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { TipoDocumento } from 'src/app/domain/models/tipo-documento';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService {

  constructor(
    private http: HttpClient
  ) { }

  listar() : Observable<Array<TipoDocumento>> {
    return this.http.get<Array<TipoDocumento>>(`${environment.API_URL}/api/tipo-documento/usuario`);
  }

  save(tipoDocumento: TipoDocumento) :Observable<any>{
    return this.http.post<TipoDocumento>(`${environment.API_URL}/api/tipo-documento`, tipoDocumento);
  }
}
