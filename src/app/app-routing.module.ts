import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tipo-documento',
    loadChildren: () => import('./modules/tipo-documento/tipo-documento.module').then(m => m.TipoDocumentoModule)
  },
  {
    path: 'tipo-contribuyente',
    loadChildren: () => import('./modules/tipo-contribuyente/tipo-contribuyente.module').then(m => m.TipoContribuyenteModule)
  },
  {
    path: 'entidad',
    loadChildren: () => import('./modules/entidad/entidad-routing.module').then(M => M.EntidadRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
