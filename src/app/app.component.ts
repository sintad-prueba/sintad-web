import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sintad-web';

  navData = navbarData;
  
}

export const navbarData = [
  {
    routeLink: 'tipo-documento',
    icon: 'pi pi-home',
    label: 'Tipo Documento'
  },
  {
    routeLink: 'tipo-contribuyente',
    icon: 'pi pi-home',
    label: 'Tipo Contribuyente'
  },
  {
    routeLink: 'entidad',
    icon: 'pi pi-home',
    label: 'Entidad'
  }
];
