import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoContribuyenteRoutingModule } from './tipo-contribuyente-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TipoContribuyenteRoutingModule
  ]
})
export class TipoContribuyenteModule { }
