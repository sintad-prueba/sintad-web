import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntidadRoutingModule } from './entidad-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EntidadRoutingModule
  ]
})
export class EntidadModule { }
