import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoDocumentoRoutingModule } from './tipo-documento-routing.module';
import { ListaComponent } from './pages/lista/lista.component';
import { RegistrarComponent } from './pages/registrar/registrar.component';

import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';


@NgModule({
  declarations: [
    ListaComponent,
    RegistrarComponent,

  ],
  imports: [
    CommonModule,
    TipoDocumentoRoutingModule,
    TableModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextModule,
    ToastModule,
  ]
})
export class TipoDocumentoModule { }
