import { Component } from '@angular/core';
import { TipoDocumentoService } from 'src/app/core/services/tipo-documento.service';
import { TipoDocumento } from 'src/app/domain/models/tipo-documento';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent {

  lista!: Array<TipoDocumento>

  constructor(
    private tipoDocumentoService: TipoDocumentoService
  ) {

  }

  ngOnInit(){
    this.tipoDocumentoService.listar().subscribe( lista => {
      this.lista = lista;
    })
  }

}
